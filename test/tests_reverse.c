/*
  tests_reverse.h
  Copyright (c) J.J. Green 2014
*/

#include <gstack.h>
#include "tests_reverse.h"
#include "tests_helper.h"

CU_TestInfo tests_reverse[] = 
  {
    {"integrity",   test_reverse_integrity},
    {"empty stack", test_reverse_empty_stack},
    CU_TEST_INFO_NULL,
  };

extern void test_reverse_integrity(void)
{
  size_t
    sizes[] = {0, 1, 2, 6, 23},
    n = sizeof(sizes)/sizeof(size_t);

  for (int k = 0 ; k < n ; k++)
    {
      size_t size = sizes[k];
      gstack_t* g;

      CU_TEST_FATAL( (g = build_gstack(size)) != NULL );
      CU_TEST_FATAL( gstack_size(g) == size );

      CU_ASSERT( gstack_reverse(g) == 0 );
      CU_ASSERT( gstack_size(g) == size );
      
      for (int i = 0 ; i < size ; i++)
	{
	  int res;
	  
	  CU_ASSERT( ! gstack_empty(g) );
	  CU_ASSERT( gstack_pop(g, (void *) &res) == 0 );
	  CU_ASSERT( res ==  i );
	}
      
      CU_ASSERT( gstack_empty(g) );
      
      gstack_destroy(g);
    }
}


extern void test_reverse_empty_stack(void)
{
  gstack_t *g = gstack_new(sizeof(int), 0, 10);

  CU_TEST_FATAL( g != NULL );
  CU_TEST_FATAL( gstack_empty(g) );

  CU_ASSERT( gstack_reverse(g) == 0 );
  CU_ASSERT( gstack_empty(g) );

  gstack_destroy(g);
}
