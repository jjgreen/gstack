/*
  tests_foreach.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_FOREACH_H
#define TESTS_FOREACH_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_foreach[]; 

extern void test_foreach_adder(void);
extern void test_foreach_shortcircuit(void);
extern void test_foreach_error(void);

#endif
