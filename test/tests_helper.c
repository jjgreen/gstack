/*
  tests_helper.c
  Copyright (c) J.J. Green 2015
*/

#include "tests_helper.h"

extern gstack_t* build_gstack(int n)
{
  gstack_t* g = gstack_new(sizeof(int), 10, 10);

  if (g == NULL)
    return NULL;

  int i;

  for (i=0 ; i<n ; i++)
    if (gstack_push(g, (void *) &i) != 0 )
      return NULL;

  return g;
}
