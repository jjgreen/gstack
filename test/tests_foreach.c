/*
  tests_foreach.h
  Copyright (c) J.J. Green 2014
*/

#include <gstack.h>
#include "tests_foreach.h"
#include "tests_helper.h"

CU_TestInfo tests_foreach[] = 
  {
    {"example iterator", test_foreach_adder},
    {"short-circuit", test_foreach_shortcircuit},
    {"error", test_foreach_error},
    CU_TEST_INFO_NULL,
  };

/*
  example iterator, adds the elements of an integer sequence 
*/

static int add_cb(int *dat, int *sum)
{
  *sum += *dat;
  return 1;
}

extern void test_foreach_adder(void)
{
  int i;

  for (i=2 ; i<7 ; i++)
    {
      gstack_t* g;
  
      CU_TEST_FATAL( (g = build_gstack(i)) != NULL );

      int sum = 0;

      CU_ASSERT( gstack_foreach(g, (int (*)(void*, void*))add_cb, &sum) == 0 );
      CU_ASSERT( sum == i*(i-1)/2 );

      gstack_destroy(g);
    }
}

/*
  check early-termination
*/

static int shortcircuit_after(int *n, int *count)
{
  return (--(*count) > 0 ? 1 : 0);
}

extern void test_foreach_shortcircuit(void)
{
  int n = 20;
  gstack_t* g;
  
  CU_TEST_FATAL( (g = build_gstack(n)) != NULL );

  int
    count = n/2,
    err = gstack_foreach(g, (int (*)(void*, void*))shortcircuit_after, &count);

  CU_ASSERT( err == 0 );
  CU_ASSERT( count == 0 );

  gstack_destroy(g);
}

/*
  check error termination
*/

static int error_after(int *n, int *count)
{
  return (--(*count) > 0 ? 1 : -3);
}

extern void test_foreach_error(void)
{
  int n = 20;
  gstack_t* g;
  
  CU_TEST_FATAL( (g = build_gstack(n)) != NULL );

  int count = n/2;
  
  CU_ASSERT( gstack_foreach(g, (int (*)(void*, void*))error_after, &count) != 0 );
  CU_ASSERT( count == 0 );

  gstack_destroy(g);
}
