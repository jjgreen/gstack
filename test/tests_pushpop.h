/*
  tests_pushpop.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_PUSHPOP_H
#define TESTS_PUSHPOP_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_pushpop[]; 

extern void test_pushpop_integrity(void);
extern void test_pushpop_pop_empty(void);

#endif
