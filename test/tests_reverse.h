/*
  tests_reverse.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_REVERSE_H
#define TESTS_REVERSE_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_reverse[]; 

extern void test_reverse_integrity(void);
extern void test_reverse_empty_stack(void);

#endif
