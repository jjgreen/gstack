/*
  tests_pushpop.h
  Copyright (c) J.J. Green 2014
*/

#include <gstack.h>
#include "tests_pushpop.h"
#include "tests_helper.h"

CU_TestInfo tests_pushpop[] = 
  {
    {"integrity",    test_pushpop_integrity},
    {"pop on empty", test_pushpop_pop_empty},
    CU_TEST_INFO_NULL,
  };

/* same out as we put in */

extern void test_pushpop_integrity(void)
{
  gstack_t* g;

  CU_TEST_FATAL( (g = build_gstack(4)) != NULL );

  int i;

  for (i=0 ; i<4 ; i++)
    {
      int res;
      CU_ASSERT( gstack_empty(g) == 0 );
      CU_ASSERT( gstack_pop(g, (void *) &res) == 0 );
      CU_ASSERT( res ==  3 - i );
    }

  CU_ASSERT( gstack_empty(g) == 1 );

  gstack_destroy(g);
}

/* popping an empty stack should return nonzero and not assign */

static void pop_on_empty(gstack_t *g)
{
  CU_TEST_FATAL( gstack_empty(g) == true );

  int res = 12345;

  CU_ASSERT( gstack_pop(g, (void *) &res) != 0 );
  CU_ASSERT( res == 12345 );
  CU_ASSERT( gstack_empty(g) == true );
}

static void pop_on_initially_empty(void)
{
  gstack_t* g;

  CU_TEST_FATAL( (g = build_gstack(0)) != NULL );
  pop_on_empty(g);
  gstack_destroy(g);
}

static void pop_on_emptied(void)
{
  gstack_t* g;
  int res = 0;
  
  CU_TEST_FATAL( (g = build_gstack(1)) != NULL );
  CU_TEST_FATAL( gstack_pop(g, (void *) &res) == 0 );
  pop_on_empty(g);
  gstack_destroy(g);  
}

extern void test_pushpop_pop_empty(void)
{
  pop_on_initially_empty();
  pop_on_emptied();
}
