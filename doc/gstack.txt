Name

   gstack — generic stack.

Synopsis

#include <gstack.h>

   gstack_t * gstack_new ( size_t size,
                           size_t initial,
                           size_t increment);

   void gstack_destroy( gstack_t * stack);

   int gstack_push( gstack_t * stack,
                    const void * item);

   int gstack_pop( gstack_t * stack,
                   void * item);

   int gstack_reverse( gstack_t * stack);

   int gstack_foreach( gstack_t * stack,
                       int (*f)(void*, void*) ,
                       void * context);

   bool gstack_empty( const gstack_t * stack);

   size_t gstack_size( const gstack_t * stack);

DESCRIPTION

   A push/pop stack implemented on a dynamic array, the item being of a
   fixed size passed to the constructor, gstack_create along with the
   initial size of the stack and the increment by which to expand it when
   needed.

   The gstack_push function copies the item onto the head of the stack,
   and likewise gstack_pop copies it back. One queries the emptiness of
   the stack with gstack_empty

   Non-stack operations on a gstack include gstack_size, giving the number
   of items on the stack, gstack_reverse for an in-place reversal of the
   stack, and gstack_foreach as a general-purpose iterator. The function
   f, of prototype
   int f( void * item,
          void * context);

   is applied to each item in the stack (in insert order); returns
   positive to continue, 0 to terminate succesfully and negative to
   terminate with error.

RETURN VALUE

   Functions returning an integer return zero on success and non-zero on
   error.

AUTHORS

   J. J. Green.
